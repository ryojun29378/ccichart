import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
 
  data = [];
  color ={};
  color2={};
  vivid = {};
  natural = {};
  cool = {};
  fire = {};

  XText;
  YText;
  resultValue: string;
  

  test_data = [
    {
      "name": "Data1",
      "value": 250,     
    },
    {
      "name": "Data2",
      "value": 100
    },
    {
      "name": "Data3",
      "value": 50
    },
    {
        "name": "Data4",
        "value": 200,
      },
      {
        "name": "Data5",
        "value": 200,
      },
      {
        "name": "Data6",
        "value": 200,
      },
      
  ];

  Data1 = [
    {
      "name": "Data1_1",
      "value": 250
    },
    {
      "name": "Data1_2",
      "value": 100
    },
    {
      "name": "Data1_3",
      "value": 50
    },
    {
      "name": "Data1_4",
      "value": 570
    },
    {
      "name": "Data1_5",
      "value": 350
    },
    {
      "name": "Data1_6",
      "value": 520
    }
    
      
  ];

  Data2= [
    {
      "name": "Data2_1",
      "value": 250
    },
    {
      "name": "Data2_2",
      "value": 340
    },
    {
      "name": "Data2_3",
      "value": 200
    },
    {
        "name": "Data2_4",
        "value": 250,
    },
    {
        "name": "Data2_5",
        "value": 450,
    },
    {
        "name": "Data2_6",
        "value": 800,
    },
      
  ];

  Data3= [
    {
      "name": "Data3_1",
      "value": 250
    },
    {
      "name": "Data3_2",
      "value": 340
    },
    {
      "name": "Data3_3",
      "value": 200
    },
          
  ];
  

  t_colorScheme = {
    domain: ['#E36969', '#E3A669', '#E3E369', '#69E3A6','#69DAE3']
  };
  t_colorScheme2 = {
    domain: ['#E36989', '#E36669', '#E3E669', '#69E4A6','#69DAE3']
  };

  t_vivid = {
    domain: [
    '#647c8a', '#3f51b5', '#2196f3', '#00b862', '#afdf0a', '#a7b61a', '#f3e562', '#ff9800', '#ff5722', '#ff4514']
  };
  t_natural = {
    domain: [
    '#bf9d76', '#e99450', '#d89f59', '#f2dfa7', '#a5d7c6', '#7794b1', '#afafaf', '#707160', '#ba9383', '#d9d5c3']
  };
  t_cool = {
    domain: [
      '#a8385d', '#7aa3e5', '#a27ea8', '#aae3f5', '#adcded', '#a95963', '#8796c0', '#7ed3ed', '#50abcc', '#ad6886']
  };
  t_fire = {
    domain: [
      '#ff3d00', '#bf360c', '#ff8f00', '#ff6f00', '#ff5722', '#e65100', '#ffca28', '#ffab00']
  };


  constructor() {

  }

  ngOnInit() {
    this.data = this.test_data;
    this.color = this.t_colorScheme;
    this.color2 = this.t_colorScheme2;
    this.vivid = this.t_vivid;
    this.natural = this.t_natural;
    this.cool = this.t_cool;
    this.fire = this.t_fire;
    
  }

  result(value){
    if(value == "Data1"){
      this.data = this.Data1;
      this.XText = "Data1X";
      this.YText = "Data1Y";
    }
    
    else if(value == "Data2"){
      this.data = this.Data2
      this.XText = "Data2X";
      this.YText = "Data2Y";
    }
    else if(value == "Data3"){
      this.data = this.Data3
      this.XText = "Data3X";
      this.YText = "Data3Y";
    }
    else if(value == "Data1_1"){
      this.data = this.test_data
      this.XText = "DataX";
      this.YText = "DataY";
    }
   /* else if(value == "Data2")
    /this.data = this.Data1_3
    else if(value == "Data2")
    this.data = this.Data1_4*/
  }

}
