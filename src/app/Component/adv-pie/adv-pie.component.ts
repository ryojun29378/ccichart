import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single, multi} from '.././data';

@Component({
  selector: 'app-adv-pie',
  template: `
    <ngx-charts-advanced-pie-chart
      [view]="view"
      [scheme]="colorScheme"
      [results]="single"
      [gradient]="gradient"
      (select)="onSelect($event)">
    </ngx-charts-advanced-pie-chart>
  `
})
export class AdvPieComponent {
  //Data type
  single: any[];
  multi: any[];
  
  //Chart size
  view: any[] = [700, 400];

  
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  //Add Data
  constructor() {
    Object.assign(this, {single, multi})   
  }
  
  
  onSelect(event) {
    console.log(event);
  }
  
}

