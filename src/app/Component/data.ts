export var single = [
    {
      "name": "Data1",
      "value": 250
    },
    {
      "name": "Data2",
      "value": 100
    },
    {
      "name": "Data3",
      "value": 50
    },
    {
        "name": "Data4",
        "value": 50,
        id: 500
      },
      
      
  ];
  
  export var multi = [
    {
      "name": "Data1",
      "series": [
        {
          "name": "2010",
          "value": 500
        },
        {
          "name": "2011",
          "value": 200
        }
      ]
    },
  
    {
      "name": "Data2",
      "series": [
        {
          "name": "2010",
          "value": 500
        },
        {
          "name": "2011",
          "value": 200
        }
      ]
    },
  
    {
      "name": "All",
      "series": [
        {
          "name": "2010",
          "value": 500
        },
        {
          "name": "2011",
          "value": 500
        }
      ]
    }
  ];
  