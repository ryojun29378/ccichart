import { Component, Input, OnInit,Output,EventEmitter} from '@angular/core';


@Component({
  selector: 'app-all-chart',
  templateUrl: './all-chart.component.html',
  styleUrls: ['./all-chart.component.css']
})
export class AllChartComponent implements OnInit {
  ngOnInit(): void {
    console.log('On init PieChartComponent')
  }
  
  
  @Input() data;
  @Input() view: any[];
  @Input() Type;
  @Input() colorScheme;
  @Input() XText;
  @Input() YText;

  @Output() LinkData: EventEmitter<string> = new EventEmitter();
  // options
  resultValue: string;

  Color = [];
  showLegend = true;
  animations = true;
  maxLabelLength = 10;
  showLabels = true;
  legendTitle = 'Lege';
  
  WgSpace = 700;
  HgSpace = 400;
  

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  xAxisLabel = this.XText;
  showYAxisLabel = true;
  yAxisLabel = this.YText;




  BarChart1: boolean = false;
  BarChartHorizon: boolean = false;
  PieChart1: boolean = false;


  constructor() {

  }
  onSelect(event) {
    this.LinkData.emit(event.name);
    console.log();
  }

  
}

