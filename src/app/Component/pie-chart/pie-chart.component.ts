import { Component, NgModule, Input, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single } from '.././data';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  ngOnInit(): void {
    console.log('On init PieChartComponent')
  }

  // @Input() set data(value) {
  //   console.log(value)
  //   this.single = value
  // }

  @Input() data;

  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // options
  
  showLegend = true;
  animations = true;
  maxLabelLength = 10;
  showLabels = true;
  legendTitle = 'Lege';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
    // Object.assign(this, { single })
  }

  onSelect(event) {
    console.log(event);
  }
}
