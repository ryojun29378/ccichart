import { Injectable } from '@angular/core';
import { Component, NgModule, Input, OnInit } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MainChartService implements OnInit{
  ngOnInit(): void {
    console.log('On init PieChartComponent')
  }

  // @Input() set data(value) {
  //   console.log(value)
  //   this.single = value
  // }

  @Input() data;



  single: any[];
  multi: any[];

  view: any[] = [700, 400];

  // options
  
  showLegend = true;
  animations = true;
  maxLabelLength = 10;
  showLabels = true;
  legendTitle = 'Lege';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor() {
    // Object.assign(this, { single })
  }

  onSelect(event) {
    console.log(event);
  }
}
