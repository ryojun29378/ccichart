import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single } from '.././data';

@Component({
  selector: 'app-bar-chart1',
  templateUrl: './bar-chart1.component.html',
  styleUrls: ['./bar-chart1.component.css']
})
export class BarChart1Component {

  // Set Text and Size
  single: any[];
  multi: any[];
  WgSpace = 700;
  HgSpace = 400;
  XText = "Name";
  YText = "Point";

  view: any[] = [this.WgSpace, this.HgSpace];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = this.XText;
  showYAxisLabel = true;
  yAxisLabel = this.YText;

  //Set Color for Scheme
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  constructor() {
    Object.assign(this, { single })
  }

  onSelect(event) {
    console.log(event);
  }
}
