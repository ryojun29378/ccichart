import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { multi } from '.././data';

@Component({
  selector: 'app-group-bar-chart',
  templateUrl: './group-bar-chart.component.html',
  styleUrls: ['./group-bar-chart.component.css']
})
export class GroupBarChartComponent {

  // Set Text and Size
  single: any[];
  multi: any[];
  WgSpace = 700;
  HgSpace = 400;
  XText = "Country";
  YText = "Population";

  view: any[] = [this.WgSpace, this.HgSpace];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = this.XText;
  showYAxisLabel = true;
  yAxisLabel = this.YText;

  //Set Color for Scheme
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  constructor() {
    Object.assign(this, { multi })
  }

  onSelect(event) {
    console.log(event);
  }
}
